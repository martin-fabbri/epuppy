const createItem = async (parent, args, ctx, info) => {
    // TODO: check if they are logged in
    const item = await ctx.db.mutation.createItem(
        {
            data: {
                ...args,
            },
        },
        info
    )
    console.log(item)
    return item
}

const updateItem = (parent, args, ctx, info) => {
    // TODO: check if they are logged in
    const updates = { ...args }
    delete updates.id
    return ctx.db.mutation.updateItem({
        data: updates,
        where: {
            id: args.id,
        },
        info,
    })
}

const deleteItem = async (parent, args, ctx, info) => {
    const where = { id: args.id }
    const requestedInfo = '{id title}'
    // 1. find the item
    const item = await ctx.db.query.item({ where }, requestedInfo)
    // 2. TODO: Check if they own that item, or have the permissions

    // 3. Delete it
    return ctx.db.mutation.deleteItem({ where }, requestedInfo)
}

const mutation = {
    createItem,
    updateItem,
    deleteItem,
}

module.exports = mutation
