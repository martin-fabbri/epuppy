const { forwardTo } = require('prisma-binding')

// const items = async (parent, args, ctx, info) => {
//     // TODO: check if they are logged in
//     console.log('Getting items!!!');
//     const items = await ctx.db.query.items();
//     console.log(items);
//     return items;
// };

const items = forwardTo('db')
const item = forwardTo('db')

const query = {
    items,
    item,
};

module.exports = query;