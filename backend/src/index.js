require('dotenv').config({path: 'variables.env'});
const createServer = require('./create-server');
const db = require('./db');

const server = createServer();
server.start({
   cors: {
       credentials: true,
       origin: process.env.FRONTEND_URL
   }
}, deets => {
    console.log(`Server running on port: ${deets.port}`);
});

console.log('ok', process.env.PRISMA_SECRET);