import React from 'react'
import Link from 'next/link'
import Title from './styles/title'
import ItemStyles from './styles/item-styles'
import PriceTag from './styles/price-tag'
import DeleteItem from './delete-item'
import formatMoney from '../lib/format-money'

const Item = props => {
    const { item } = props
    return (
        <ItemStyles>
            {item.image && <img src={item.image} alt={item.title}/>}
            <Title>
                <Link href={{ pathname: '/item', query: { id: item.id } }}>
                    <a>{item.title}</a>
                </Link>
            </Title>
            <PriceTag>{formatMoney(item.price)}</PriceTag>
            <p>{item.description}</p>
            <div className="buttonList">
                <Link href={{ pathname: 'update', query: { id: item.id } }}>
                    <a>Edit</a>
                </Link>
                <Link>
                    <a>Add to Cart</a>
                </Link>
                <DeleteItem id={item.id}>Delete</DeleteItem>
            </div>
        </ItemStyles>
    )
}

export default Item
