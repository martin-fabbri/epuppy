import React from 'react'
import Header from './header'
import Meta from './meta'
import styled, { createGlobalStyle, ThemeProvider } from 'styled-components'

const theme = {
    red: '#FF0000',
    black: '#393939',
    grey: '#3A3A3A',
    lightgrey: '#E1E1E1',
    offWhite: '#EDEDED',
    maxWidth: '1000px',
    bs: '0 12px 24px 0 rgba(0, 0, 0, 0.09)',
    baseFontSize: '10px',
}

const GlobalStyle = createGlobalStyle`  
  html {
    box-sizing: border-box;
    font-size: ${ props => props.theme.baseFontSize };
  }
  
  *, *:before, *:after {
    @import url('/static/radnikanext-medium-webfont.woff2');
    font-family: 'radnika_next', serif;
    font-weight: normal;
    font-style: normal;
    box-sizing: inherit;
  }
  
  body {
    margin: 0;
    padding: 0;
    font-size: 1.5rem;
    line-height: 2;
  }
  
  a {
    text-decoration: none;
    color:  ${ props => props.theme.black };
  }
`;

const PageContainer = styled.div`
  background: white;
  color: ${props => props.theme.black};
`

const Inner = styled.div`
  max-width: ${props => props.theme.maxWidth};
  margin: 0 auto;
  padding: 2rem;
`;

const Page = props => {
    const { children } = props
    return (
        <ThemeProvider theme={theme}>
            <PageContainer>
                <GlobalStyle />
                <Meta />
                <Header />
                <Inner>
                    {children}
                </Inner>
            </PageContainer>
        </ThemeProvider>
    )
}

export default Page
