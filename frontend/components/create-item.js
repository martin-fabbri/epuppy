import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import Router from 'next/router'
import Form from './styles/form';
import Error from './error-message';

export const CREATE_ITEM_MUTATION = gql`
    mutation CREATE_ITEM_MUTATION(
        $title: String!
        $description: String!
        $price: Int!
        $image: String
        $largeImage: String
    ) {
        createItem(
            title: $title
            description: $description
            price: $price
            image: $image
            largeImage: $largeImage
        ) {
            id
        }
    }
`

const CreateItem = () => {
    const [form, setForm] = useState({
        title: '',
        description: '',
        price: 0,
    })

    const handleFormChange = (e) => {
        const { name, type, value  } = e.target
        const val = type === 'number' ? parseFloat(value) : value
        setForm({
            ...form,
            [name]: val,
        })
    }

    const uploadFile = async e => {
        console.log('uploading a file')
        const files = e.target.files
        const data = new FormData();
        data.append('file', files[0])
        data.append('upload_preset', 'epuppy')
        const res = await fetch('https://api.cloudinary.com/v1_1/dgxzgs8fd/image/upload', {
            method: 'POST',
            body: data
        })
        const file = await res.json()
        console.log(file)
        setForm({
            ...form,
            image: file.secure_url,
            largeImage: file.eager[0].secure_url
        })
    }

    return (
        <Mutation mutation={CREATE_ITEM_MUTATION} variables={form}>
            {(createItem, { loading, error }) => (
                <Form onSubmit={async e => {
                    e.preventDefault()
                    const res = await createItem()
                    console.log(res)
                    Router.push({
                        pathname: '/item',
                        query: {id: res.data.createItem.id}
                    })
                }}>
                    <h2>Sell an Item.</h2>
                    <Error error={error}/>
                    <fieldset disabled={loading} aria-busy={loading}>
                        <label htmlFor="title">
                            Image
                            <input
                                type="file"
                                id="file"
                                name="file"
                                placeholder="Upload an image"
                                required
                                onChange={uploadFile}
                            />
                            {form.image && <img src={form.image} width={200} alt="Upload preview"/>}
                        </label>
                        <label htmlFor="title">
                            Title
                            <input
                                type="text"
                                id="title"
                                name="title"
                                placeholder="Title"
                                required
                                value={form.title}
                                onChange={handleFormChange}
                            />
                        </label>
                        <label htmlFor="price">
                            Price
                            <input
                                type="number"
                                id="price"
                                name="price"
                                placeholder="Price"
                                required
                                value={form.price}
                                onChange={handleFormChange}
                            />
                        </label>
                        <label htmlFor="description">
                            Description
                            <textarea
                                type="text"
                                id="description"
                                name="description"
                                placeholder="Enter a description"
                                required
                                value={form.description}
                                onChange={handleFormChange}
                            />
                        </label>
                        <button type="submit">Submit</button>
                    </fieldset>
                </Form>
            )}
        </Mutation>
    )
}

export default CreateItem
