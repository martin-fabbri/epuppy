import React, { useState } from 'react'
import { Mutation, Query } from 'react-apollo'
import gql from 'graphql-tag'
import Router from 'next/router'
import Form from './styles/form'
import Error from './error-message'

export const SINGLE_ITEM_QUERY = gql`
    query SINGLE_ITEM_QUERY($id: ID!) {
        item(where: { id: $id }) {
            id
            title
            description
            price
        }
    }
`

export const UPDATE_ITEM_MUTATION = gql`
    mutation UPDATE_ITEM_MUTATION(
        $id: ID!
        $title: String
        $description: String
        $price: Int
    ) {
        updateItem(id: $id, title: $title, description: $description, price: $price) {
            id
            title
            description
            price
        }
    }
`

const UpdateItem = props => {
    const [form, setForm] = useState({})
    const { id } = props

    const handleFormChange = e => {
        const { name, type, value } = e.target
        const val = type === 'number' ? parseFloat(value) : value
        setForm({
            ...form,
            [name]: val,
        })
    }

    const handleUpdateItem = async (e, updateItemMutation) => {
        e.preventDefault()
        console.log('updating item')
        console.log(form)
        const res = await updateItemMutation({
            variables: {
                id: props.id,
                ...form,
            }
        })
    }

    return (
        <Query query={SINGLE_ITEM_QUERY} variables={{ id }}>
            {({ data, loading }) => {
                if (loading) return <p>Loading...</p>
                if (!data || !data.item) return <p>No item found.</p>
                return (
                    <Mutation mutation={UPDATE_ITEM_MUTATION} variables={form}>
                        {(updateItem, { loading, error }) => (
                            <Form onSubmit={e => handleUpdateItem(e, updateItem)}>
                                <h2>Sell an Item.</h2>
                                <Error error={error} />
                                <fieldset
                                    disabled={loading}
                                    aria-busy={loading}
                                >
                                    <label htmlFor="title">
                                        Title
                                        <input
                                            type="text"
                                            id="title"
                                            name="title"
                                            placeholder="Title"
                                            required
                                            defaultValue={data.item.title}
                                            onChange={handleFormChange}
                                        />
                                    </label>
                                    <label htmlFor="price">
                                        Price
                                        <input
                                            type="number"
                                            id="price"
                                            name="price"
                                            placeholder="Price"
                                            required
                                            defaultValue={data.item.price}
                                            onChange={handleFormChange}
                                        />
                                    </label>
                                    <label htmlFor="description">
                                        Description
                                        <textarea
                                            type="text"
                                            id="description"
                                            name="description"
                                            placeholder="Enter a description"
                                            required
                                            defaultValue={data.item.description}
                                            onChange={handleFormChange}
                                        />
                                    </label>
                                    <button type="submit">Save Changes</button>
                                </fieldset>
                            </Form>
                        )}
                    </Mutation>
                )
            }}
        </Query>
    )
}

export default UpdateItem
