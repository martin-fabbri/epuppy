import React from 'react'
import gql from 'graphql-tag'
import { Query } from 'react-apollo'
import styled from 'styled-components'
import Error from './error-message'
import Head from 'next/head'

const SingleItemContainer = styled.div`
    max-width: 1200px;
    margin: 2rem auto;
    box-shadow: ${props => props.theme.bs};
    display: grid;
    grid-auto-columns: 1fr;
    grid-auto-flow: column;
    min-height: 800px;
    img {
      width: 100%;
      height: 100%;
      // object-fit: cover;
      object-fit: contain;
    }
`

const Details = styled.div`
  margin: 3rem;
  font-size: 2rem;
`

const SINGLE_ITEM_QUERY = gql`
    query SINGLE_ITEM_QUERY($id: ID!) {
        item(where: { id: $id }) {
            id
            title
            description
            price
            image
            largeImage
        }
    }
`

const SingleItem = props => {
    return (
        <Query query={SINGLE_ITEM_QUERY} variables={{ id: props.id }}>
            {({ error, loading, data }) => {
                if (error) return <Error error={error} />
                if (loading) return <p>Loading ...</p>
                if (!data.item) return <p>No item found</p>
                const {title, description, largeImage} = data.item
                return (
                    <SingleItemContainer>
                        <Head>
                            <title>ePuppy | {title}</title>
                        </Head>
                        {largeImage && <img src={largeImage} alt={title} />}
                        <Details>
                            <h2>Viewing {title}</h2>
                            <p>{description}</p>
                        </Details>
                    </SingleItemContainer>
                )
            }}
        </Query>
    )
}

export default SingleItem
