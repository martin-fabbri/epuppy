import React from 'react'
import Nav from './nav'
import Link from 'next/link'
import styled from 'styled-components'
import Router from 'next/router'
import NProgress from 'nprogress'

Router.onRouteChangeStart = () => {
    console.log('onRouteChangeStart')
    NProgress.start()
}

Router.onRouteChangeComplete = () => {
    console.log('onRouteChangeComplete')
    NProgress.done()
}

Router.onRouteChangeError = () => {
    console.log('onRouteChangeError')
    NProgress.done()
}

const Logo = styled.h1`
  font-size: 4rem;
  margin-left: 2rem;
  position: relative;
  z-index: 2;
  transform: skew(-7deg);
  a {
    padding: 0.5rem 1rem;
    background: ${props => props.theme.red};
    color: ${props => props.theme.offWhite};
  }
  @media only screen and (max-width: 768px) {
    margin: 0;
    text-align: center;
  }
`

const HeaderContainer = styled.header`
  border-bottom: 10px solid ${props => props.theme.black};
  display: grid;
  grid-template-columns: auto 1fr;
  justify-content: space-between;
  align-items: stretch;
  @media only screen and (max-width: 768px) {
    grid-template-columns: 1fr;
    justify-content: center;
  }
`
const SubBarContainer = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;
  border-bottom: 1px solid ${props => props.theme.lightgrey};
`

const Header = () => (
    <>
        <HeaderContainer className="bar">
            <Logo>
                <Link href="/">
                    <a>eQL</a>
                </Link>
            </Logo>
            <Nav />
        </HeaderContainer>
        <SubBarContainer className="sub-bar">
            <p>Search</p>
        </SubBarContainer>
        <div>Cart</div>
    </>
)

export default Header
