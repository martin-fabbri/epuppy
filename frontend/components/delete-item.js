import React from 'react'
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'
import { ALL_ITEMS_QUERY } from './items'

const DELETE_ITEM_MUTATION = gql`
    mutation DELETE_ITEM_MUTATION($id: ID!) {
        deleteItem(id: $id) {
            id
        }
    }
`

const DeleteItem = props => {
    const handleDeleteConfirmation = (e, deleteItem) => {
        if (confirm('Are you sure you want to delete this item?')) {
            deleteItem()
        }
    }

    const handleUpdate = (cache, payload) => {
        // 1. read the cache for the items we want
        const data = cache.readQuery({query: ALL_ITEMS_QUERY})
        console.log(data, payload)
        // 2. filter the deleted item out of the page
        data.items = data.items.filter(item => item.id !== payload.data.deleteItem.id)
        // 3. put the items back
        cache.writeQuery({query: ALL_ITEMS_QUERY, data})
    }

    return (
        <Mutation
            mutation={DELETE_ITEM_MUTATION}
            variables={{ id: props.id }}
            update={handleUpdate}
        >
            {(deleteItem, { error }) => (
                <button onClick={(e) => handleDeleteConfirmation(e, deleteItem)}>
                    {props.children}
                </button>
            )}
        </Mutation>
    )
}

export default DeleteItem
