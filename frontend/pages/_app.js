import App, {Container} from 'next/app';
import Page from '../components/Page';
import { ApolloProvider } from 'react-apollo'
import withData from '../lib/with-data'

const CustomApp = (props) => {
    const { apollo, Component, pageProps } = props;

    return (
        <Container>
            <ApolloProvider client={apollo}>
                <Page>
                    <Component {...pageProps} />
                </Page>
            </ApolloProvider>
        </Container>
    )
};

CustomApp.getInitialProps = async ({Component, ctx}) => {
    let pageProps = {}
    if (Component.getInitialProps) {
        pageProps = await Component.getInitialProps(ctx)
    }
    pageProps.query = ctx.query
    return { pageProps }
}


export default withData(CustomApp);