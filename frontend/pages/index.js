import React from 'react'
import Items from '../components/items'

const Home = () => (
    <>
        <Items />
    </>
);

export default Home;