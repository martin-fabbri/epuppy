import React from 'react'
import UpdateItem from '../components/update-item'

const Update = ({query}) => (
    <UpdateItem  id={query.id}/>
);

export default Update;